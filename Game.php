<?php

class Game
{
	private $teamAinfo;
	private $teamBinfo;

	public function __construct(Team $teamA, Team $teamB)
	{
		// build the info objects that will represent the teams
		$this->teamAinfo = new TeamGameInfo($teamA);
		$this->teamBinfo = new TeamGameInfo($teamB);
	}

	public function getTeamInfos()
	{
		return [
			$this->teamAinfo,
			$this->teamBInfo
		];
	}

	/**
	 * @return Team the team with the most goals, else null
	 */
	public function getWinner()
	{
		$numTeamAGoals = $this->teamAinfo->getNumGoals();
		$numTeamBGoals = $this->teamBinfo->getNumGoals();

		// Assume a draw, and reassign the winner in the case where
		// it is not proven to be a draw
		$winningTeam = null;
		if ($numTeamAGoals > $numTeamBGoals)
			$winningTeam =  $this->teamAinfo->getTeam();
		else if ($numTeamBGoals > $numTeamAGoals)
			$winningTeam = $this->teamBInfo->getTeam();
		
		return $winningTeam;
	}


	/**
	 * @return int the total number of goals scored by all teams
	 */
	public function getNumGoals()
	{
		return $this->teamAinfo->getNumGoals() + $this->teamBinfo->getNumGoals();
	}

	public function addGoal($player)
	{
		// find the team that the player relates to
		$team = $player->getTeam();

		// find the info object for that team in this game
		$teamGameInfo = $this->teamAinfo->isForTeam($team)
			? $this->teamAinfo
			: $this->teamBinfo;

		// use it to record the goal!
		$teamGameInfo->addGoal($player);
	}
}
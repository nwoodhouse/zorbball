<?php

class Team
{
	private $players;
	private $name;

	public function __construct($name)
	{
		$this->name = $name;
		$players = [];
	}

	public function getName()
	{
		return $this->name;
	}

	public function addPlayer(Player $player)
	{
		$this->players[] = $player;

		// ensure that the player knows that they have been added
		// to this, the greatest team of them all!!
		$player->setTeam($this);
	}

	/**
	 * @param $players Player[] Numerically indexed array of players
	 */
	public function addPlayers($players)
	{
		$this->players = array_merge($this->players, $players);
	}

	public function getNumPlayers()
	{
		return count($this->players);
	}

	public function getPlayers()
	{
		return $this->players;
	}
}







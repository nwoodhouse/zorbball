<?php

/**
 * Captures information about a particular Team's peformance, in a
 * particular Game.
 */
class TeamGameInfo
{
	private $team;

	// An array of all the players that have scored goals. Where
	// a player scores multiple goals, they will appear multiple
	// times in this array (not a set)
	private $scorers;

	public function __construct($team)
	{
		$this->team = $team;
	}

	/**
	 * @return bool true if this info object, relates to the given team
	 */
	public function isForTeam($team)
	{
		return $team == $this->team;
	}


	public function addGoal($player)
	{
		$this->scorers[] = $player;
	}

	public function getNumGoals()
	{
		return count($this->scorers);
	}

	public function getTeam()
	{
		return $this->team;
	}

	public function getGoals()
	{
		return $this->scorers;
	}
}
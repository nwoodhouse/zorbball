<!doctype html>
<html lang="en">

  <?php

    // bring in dependencies
    require_once('Player.php');
    require_once('Team.php');
    require_once('Game.php');
    require_once('TeamGameInfo.php');

    $player1 = new Player('Gareth Bale', 29);
    $team1 = new Team('Zorb United');
    $team1->addPlayer($player1);

    $player2 = new Player('Buzz Lightzorb', 50);
    $team2 = new Team('Zorb City');
    $team2->addPlayer($player2);

    $game = new Game($team1, $team2);

    // imagine that a goal has been scored - update the Game!
    $game->addGoal($player2);
    $game->addGoal($player2);
    $game->addGoal($player1);
    $game->addGoal($player1);
    $game->addGoal($player1);

    $totalNumberOfGoals = $game->getNumGoals();
    $winningTeam = $game->getWinner();
  ?>

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>

    <div class="container-fluid">

        <?php require('widgets/game-stats.php') ?>

        <!-- Size the team info according to our design! -->

        <div class="row">
            <div class="col-md-6">
                <?php require('widgets/team-info.php') ?>
            </div>

            <div class="col-md-6">
                <?php require('widgets/team-info.php') ?>
            </div>
        </div>

        
      
    </div>





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>